FROM redhat/ubi8:8.6
# WORKDIR /app
# COPY api /app
# RUN pip install fastapi uvicorn
RUN yum -y install git iproute bind-utils net-tools 

# CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
