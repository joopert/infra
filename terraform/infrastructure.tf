terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.6"
    }
  }
  backend "s3" {
    bucket = "terraform-statefile-8532dbe7-8a32-45e8-9d78-3ae203d36d2c"
    key    = "terraform/fastapi/infra/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_vpc" "fastapi_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "fastapi_subnet_zoneA" {
  vpc_id            = aws_vpc.fastapi_vpc.id
  cidr_block        = "10.0.0.0/20"
  availability_zone = "us-west-2a"
}
resource "aws_subnet" "fastapi_subnet_zoneB" {
  vpc_id            = aws_vpc.fastapi_vpc.id
  cidr_block        = "10.0.16.0/20"
  availability_zone = "us-west-2b"
}
resource "aws_subnet" "fastapi_subnet_zoneC" {
  vpc_id            = aws_vpc.fastapi_vpc.id
  cidr_block        = "10.0.32.0/20"
  availability_zone = "us-west-2c"
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.fastapi_vpc.id
}

resource "aws_default_route_table" "access_to_internet" {
  default_route_table_id = aws_vpc.fastapi_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_security_group" "allow_api_access" {
  name        = "Allow FastAPI inbound"
  description = "Allow FastAPI inbound traffic"
  vpc_id      = aws_vpc.fastapi_vpc.id

  ingress {
    description = "Forward to fastapi"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Allow traffic to internet"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_ecs_cluster" "fastapi_ecs_cluster" {
  name = "fastapi_ecs_cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "fastapi_ecs_capacity_provider" {
  cluster_name = aws_ecs_cluster.fastapi_ecs_cluster.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 0
    weight            = 1
    capacity_provider = "FARGATE"
  }
}

resource "aws_ecs_task_definition" "fastapi_service" {
  family                   = "fastapi_task"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"


  container_definitions = jsonencode([
    {
      name      = "FastAPI"
      image     = "registry.gitlab.com/joopert/infra"
      cpu       = 0
      essential = true
      portMappings = [
        {
          name          = "fastapi-80-tcp"
          containerPort = 80
          hostPort      = 80
          protocol      = "tcp"
        }
      ]
    }
  ])
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
}

resource "aws_ecs_service" "fastapi_service" {
  name            = "fastapi_service"
  cluster         = aws_ecs_cluster.fastapi_ecs_cluster.id
  task_definition = aws_ecs_task_definition.fastapi_service.arn
  desired_count   = 1

  network_configuration {
    subnets          = [aws_subnet.fastapi_subnet_zoneA.id, aws_subnet.fastapi_subnet_zoneB.id, aws_subnet.fastapi_subnet_zoneC.id]
    security_groups  = [aws_security_group.allow_api_access.id]
    assign_public_ip = true
  }
}
